<?php

namespace Drupal\clickandpledge_ubercart\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use SimpleXMLElement;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * To perform AJAX Operations.
 */
class CnPUberController extends ControllerBase {

  /**
   * The SOAP version.
   */
  const CFCNP_SOAP_VER = 'SOAP_1_1';

  /**
   * The Config.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactoryStorage;

  /**
   * The Connection.
   *
   * @var mixed
   */
  protected $connection;

  /**
   * The class Constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(Connection $connection, ConfigFactoryInterface $config_factory) {
    $this->connection = $connection;
    $this->configFactoryStorage = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'), $container->get('config.factory')
    );
  }

  /**
   * To get Active payment list based on account id.
   *
   * @param mixed $variable
   *   Account Id.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Return all accounts with JSON format.
   */
  public function ajaxCallback($variable) {
    $data = $this->getWCCnPactivePaymentList($variable);
    return new JsonResponse($data);
  }

  /**
   * To get Active payment list based on account id.
   *
   * @param mixed $cnpaccid
   *   The Account ID.
   *
   * @return Object
   *   Returns an object with account information.
   */
  public function getWCCnPactivePaymentList($cnpaccid) {
    $cnpacountid = $cnpaccid;
    $cnpaccountGUID = $this->getwcCnPAccountGUID($cnpacountid);
    $cnpUID = '14059359-D8E8-41C3-B628-E7E030537905';
    $cnpKey = '5DC1B75A-7EFA-4C01-BDCD-E02C536313A3';
    $connect1 = [
      'soap_version' => self::CFCNP_SOAP_VER,
      'trace' => 1,
      'exceptions' => 0,
    ];
    $client1 = new \SoapClient('https://resources.connect.clickandpledge.com/wordpress/Auth2.wsdl', $connect1);
    if (isset($cnpacountid) && $cnpacountid !== '' && isset($cnpaccountGUID) && $cnpaccountGUID !== '') {
      $xmlr1 = new SimpleXMLElement('<GetAccountDetail></GetAccountDetail>');
      $xmlr1->addChild('accountId', $cnpacountid);
      $xmlr1->addChild('accountGUID', $cnpaccountGUID);
      $xmlr1->addChild('username', $cnpUID);
      $xmlr1->addChild('password', $cnpKey);
      $response1 = $client1->GetAccountDetail($xmlr1);
      return $response1;
    }
  }

  /**
   * To get account GUID based on account id.
   *
   * @param mixed $accid
   *   The account id.
   *
   * @return mixed
   *   Returns account GUID.
   */
  public function getwcCnPAccountGUID($accid) {
    $cnpAccountGUId = '';
    $query = $this->connection->query('SELECT cnpaccountsinfo_accountguid FROM {dp_cnp_uber_jbcnpaccountsinfo} where cnpaccountsinfo_orgid =:accid', [':accid' => $accid]);
    $result = $query->fetchAssoc();
    $cnpAccountGUId = $result['cnpaccountsinfo_accountguid'];
    return $cnpAccountGUId;
  }

  /**
   * To get email account lists based on account id.
   *
   * @param mixed $cnpacid
   *   The account Id.
   *
   * @return mixed
   *   Returns users email.
   */
  public function cnp_getCnPUserEmailAccountList($cnpacid) {
    $cnpwcaccountid = $cnpacid;
    $totRes = [];

    $cnprtrntxt = $this->getwcCnPConnectCampaigns($cnpwcaccountid);
    $totRes[] = $cnprtrntxt;
    $Clist = [];
    $cnprtrnpaymentstxt = $this->getWCCnPactivePaymentList($cnpwcaccountid);
    foreach ($cnprtrnpaymentstxt as $obj => $cli) {
      foreach ($cli as $key => $value) {
        if ($value === 1) {
          $Clist[$key] = $key;
        }
      }
    }
    $totRes[] = $Clist;
    return $totRes;
  }

  /**
   * To get CONNECT campaigns.
   *
   * @param mixed $cnpaccid
   *   The account id.
   *
   * @return array
   *   Returns an array with all campaigns.
   */
  public function getwcCnPConnectCampaigns($cnpaccid) {

    $cnpacountid = $cnpaccid;
    $cnpaccountGUID = $this->getwcCnPAccountGUID($cnpacountid);
    $cnpUID = '14059359-D8E8-41C3-B628-E7E030537905';
    $cnpKey = '5DC1B75A-7EFA-4C01-BDCD-E02C536313A3';
    $connect = [
      'soap_version' => self::CFCNP_SOAP_VER,
      'trace' => 1,
      'exceptions' => 0,
    ];
    $client = new \SoapClient('https://resources.connect.clickandpledge.com/wordpress/Auth2.wsdl', $connect);
    if (isset($cnpacountid) && $cnpacountid !== '' && isset($cnpaccountGUID) && $cnpaccountGUID !== '') {
      $xmlr = new SimpleXMLElement('<GetActiveCampaignList2></GetActiveCampaignList2>');
      $xmlr->addChild('accountId', $cnpacountid);
      $xmlr->addChild('AccountGUID', $cnpaccountGUID);
      $xmlr->addChild('username', $cnpUID);
      $xmlr->addChild('password', $cnpKey);

      $response = $client->GetActiveCampaignList2($xmlr);
      $responsearr = $response->GetActiveCampaignList2Result->connectCampaign;
    }
    else {
      $responsearr = (object) ['' => 'No Campaigns Found'];
    }
    return $responsearr;
  }

  /**
   * To get updated data from click and pledge CONNECT platform.
   *
   * @param mixed $variable
   *   The account id.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Returns JSON data with all accounts.
   */
  public function getRefreshAccounts($variable) {
    $totalAccounts = [];
    $rtnrefreshtokencnpdata = $this->getRefreshToken();
    $curl = curl_init();
    curl_setopt_array($curl, [
      CURLOPT_URL => 'https://aaas.cloud.clickandpledge.com/IdServer/connect/token',
      CURLOPT_RETURNTRANSFER => TRUE,
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS => $rtnrefreshtokencnpdata,
      CURLOPT_HTTPHEADER => [
        'cache-control: no-cache',
        'content-type: application/x-www-form-urlencoded',
      ],
    ]);

    $response = curl_exec($curl);
    $err = curl_error($curl);
    if ($err) {
      echo 'cURL Error #:' . $err;
    }
    else {
      $cnptokendata = json_decode($response);

      $cnptoken = (isset($cnptokendata->access_token)) ? $cnptokendata->access_token : '';
      $cnprtokentyp = (isset($cnptokendata->token_type)) ? $cnptokendata->token_type : '';
      if ($cnptoken !== '') {
        $curl = curl_init();

        curl_setopt_array($curl, [
          CURLOPT_URL => 'https://api.cloud.clickandpledge.com/users/accountlist',
          CURLOPT_RETURNTRANSFER => TRUE,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => [
            'accept: application/json',
            "authorization: " . $cnprtokentyp . " " . $cnptoken,
            'content-type: application/json',
          ],
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
          echo 'cURL Error #:' . $err;
        }
        else {

          $cnpAccountsdata = json_decode($response);
          $rtncnpdata = $this->delete_cnpaccountslist();

          $confaccno = $variable;
          foreach ($cnpAccountsdata as $cnpkey => $cnpvalue) {
            $acc = [];
            // Data come form service-insert data into accounts list table.
            $cnporgid = $cnpvalue->OrganizationId;
            $cnporgname = addslashes($cnpvalue->OrganizationName);
            $cnpaccountid = $cnpvalue->AccountGUID;
            $cnpufname = addslashes($cnpvalue->UserFirstName);
            $cnplname = addslashes($cnpvalue->UserLastName);
            $cnpuid = $cnpvalue->UserId;
            $rtncnpdata = $this->insert_cnpwcaccountslist($cnporgid, $cnporgname, $cnpaccountid, $cnpufname, $cnplname, $cnpuid);
            $acc['orgid'] = $cnporgid;
            $acc['orgname'] = $cnporgname;
            $totalAccounts[] = $acc;
          }
        }
      }
    }

    return new JsonResponse($totalAccounts);
  }

  /**
   * To get token string.
   *
   * @return string
   *   Returns a token.
   */
  public function getRefreshToken() {
    $query = $this->connection->query("SELECT cnptokeninfo_refreshtoken FROM {dp_cnp_uber_jbcnptokeninfo}");
    $result = $query->fetchAssoc();
    $refreshtoken = $result['cnptokeninfo_refreshtoken'];
    // Settings table data.
    $sql1 = "SELECT cnpsettingsinfo_clentsecret,cnpsettingsinfo_clientid,cnpsettingsinfo_scope FROM {dp_cnp_uber_jbcnpsettingsinfo}";
    $query1 = $this->connection->query($sql1);
    $result1 = $query1->fetchAssoc();
    $password = 'password';
    $cnpsecret = openssl_decrypt($result1['cnpsettingsinfo_clentsecret'], "AES-128-ECB", $password);
    $rtncnpdata = "client_id=" . $result1['cnpsettingsinfo_clientid'] . "&client_secret=" . $cnpsecret . "&grant_type=refresh_token&scope=" . $result1['cnpsettingsinfo_scope'] . "&refresh_token=" . $refreshtoken;
    return $rtncnpdata;
  }

  /**
   * To delete click and pledge account list from our table.
   */
  public function delete_cnpaccountslist() {
    $this->connection->delete('dp_cnp_uber_jbcnpaccountsinfo')->execute();
  }

  /**
   * To save account information.
   *
   * @param mixed $cnporgid
   *   The Orgnization id.
   * @param mixed $cnporgname
   *   Organization name.
   * @param mixed $cnpaccountid
   *   Account id.
   * @param mixed $cnpufname
   *   Account first name.
   * @param mixed $cnplname
   *   Account last name.
   * @param mixed $cnpuid
   *   User id.
   *
   * @return mixed
   *   Returns inserted record id.
   */
  public function insert_cnpwcaccountslist($cnporgid, $cnporgname, $cnpaccountid, $cnpufname, $cnplname, $cnpuid) {
    $this->connection->insert('dp_cnp_uber_jbcnpaccountsinfo')
      ->fields([
        'cnpaccountsinfo_orgid' => $cnporgid,
        'cnpaccountsinfo_orgname' => $cnporgname,
        'cnpaccountsinfo_accountguid' => $cnpaccountid,
        'cnpaccountsinfo_userfirstname' => $cnpufname,
        'cnpaccountsinfo_userlastname' => $cnplname,
        'cnpaccountsinfo_userid' => $cnpuid,
      ])
      ->execute();
    $res = $this->connection->query("select max(cnpaccountsinfo_id) from {dp_cnp_uber_jbcnpaccountsinfo}");
    $id = $res->fetchCol();
    return $id[0];
  }

  /**
   * Sign in with different user.
   */
  public function signinDifferentUser() {
    // $this->configFactory()->getEditable('cnpuber.settings')->delete();
    $this->configFactoryStorage->getEditable('cnpuber.settings')->delete();
    // $this->config('cnpuber.settings')->delete();
    $this->clickandpledge_ubercart_goto('cnpauth');
  }

  /**
   * Page redirection.
   *
   * @param mixed $path
   *   Pathe to redirect.
   */
  public function clickandpledge_ubercart_goto($path) {
    $response = new RedirectResponse($path, 302);
    $response->send();
  }

}
