<?php

namespace Drupal\clickandpledge_ubercart\Form;

use SimpleXMLElement;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Click & Pledge payment settings form.
 */
class CnPUberSettings extends ConfigFormBase {

  /**
   * The Module Handler.
   *
   * @var mixed
   */
  protected $moduleHandler;

  /**
   * Stores the configuration for the current module.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configModule;

  /**
   * The Database connection.
   *
   * @var mixed
   */
  protected $connection;

  /**
   * The class Constructor.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The Module handler.
   * @param \Drupal\Core\Database\Connection $connection
   *   The Database connection.
   */
  public function __construct(ModuleHandlerInterface $module_handler, Connection $connection) {
    $this->moduleHandler = $module_handler;
    $this->connection = $connection;
    $query = $this->connection->query("SELECT cnpaccountsinfo_id FROM {dp_cnp_uber_jbcnpaccountsinfo}");
    $query->allowRowCount = TRUE;
    if ($query->rowCount() === 0) {
      $this->clickandpledge_ubercart_goto('cnpauth');
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler'), $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'cnpuber.mainsettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cnp_main_settings';
  }

  /**
   * Click & Pledge ubercart payment settings form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param mixed $form_state
   *   The current state of the form.
   *
   * @return array
   *   Returns The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $this->configFactory()->getEditable('cnpuber.settings')->delete();
    $config = $this->config('cnpuber.mainsettings');
    $form = $this->displaySettingsForm($form, $config, $form_state);
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Form validations are handled by using javascript.
  }

  /**
   * Page Redirection.
   *
   * @param mixed $path
   *   Path to redirect.
   *
   * @return mixed
   *   Nothing return.
   */
  public function clickandpledge_ubercart_goto($path) {
    $response = new RedirectResponse($path, 302);
    $response->send();
    return;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config('cnpuber.mainsettings')
      ->set('cnpuber.cnp_status_enable', $form_state->getValue('cnp_status_enable'))
      ->set('cnpuber.cnp_accid', $form_state->getValue('cnp_accid'))
      ->set('cnpuber.cnp_camp_urls', $form_state->getValue('cnp_camp_urls'))
      ->set('cnpuber.cnp_mode', $form_state->getValue('cnp_mode'))
      ->set('cnpuber.cnp_payment_credit_cards', $form_state->getValue('cnp_payment_credit_cards'))
      ->set('cnpuber.cnp_payment_credit_card_options_hidden', $form_state->getValue('cnp_payment_credit_card_options_hidden'))
      ->set('cnpuber.cnp_pre_auth', $form_state->getValue('cnp_pre_auth'))
      ->set('cnpuber.cnp_receipt_patron', $form_state->getValue('cnp_receipt_patron'))
      ->set('cnpuber.cnp_receipt_header', trim($form_state->getValue('cnp_receipt_header')))
      ->set('cnpuber.cnp_terms_con', trim($form_state->getValue('cnp_terms_con')))
      ->set('cnpuber.cnp_recurr_label', trim($form_state->getValue('cnp_recurr_label')))
      ->set('cnpuber.cnp_recurr_settings', trim($form_state->getValue('cnp_recurr_settings')))
      ->set('cnpuber.cnp_recurr_oto', $form_state->getValue('cnp_recurr_oto'))
      ->set('cnpuber.cnp_recurr_recur', $form_state->getValue('cnp_recurr_recur'))
      ->set('cnpuber.cnp_default_payment_options', $form_state->getValue('cnp_default_payment_options'))
      ->set('cnpuber.cnp_recurring_types', trim($form_state->getValue('cnp_recurring_types')))
      ->set('cnpuber.cnp_recurr_type_option', $form_state->getValue('cnp_recurr_type_option'))
      ->set('cnpuber.cnp_default_recurring_type', $form_state->getValue('cnp_default_recurring_type'))
      ->set('cnpuber.cnp_recurring_periodicity', trim($form_state->getValue('cnp_recurring_periodicity')))
      ->set('cnpuber.cnp_recurring_periodicity_options', $form_state->getValue('cnp_recurring_periodicity_options'))
      ->set('cnpuber.cnp_recurring_no_of_payments', trim($form_state->getValue('cnp_recurring_no_of_payments')))
      ->set('cnpuber.cnp_recurring_no_of_payments_options', $form_state->getValue('cnp_recurring_no_of_payments_options'))
      ->set('cnpuber.cnp_recurring_default_no_payment_lbl', trim($form_state->getValue('cnp_recurring_default_no_payment_lbl')))
      ->set('cnpuber.cnp_recurring_default_no_payments', trim($form_state->getValue('cnp_recurring_default_no_payments')))
      ->set('cnpuber.cnp_recurring_max_no_payment_lbl', trim($form_state->getValue('cnp_recurring_max_no_payment_lbl')))
      ->set('cnpuber.cnp_recurring_max_no_payment', trim($form_state->getValue('cnp_recurring_max_no_payment')))
      ->set('cnpuber.cnp_recurring_default_no_payment_open_field_lbl', trim($form_state->getValue('cnp_recurring_default_no_payment_open_field_lbl')))
      ->set('cnpuber.cnp_recurring_default_no_payments_open_filed', trim($form_state->getValue('cnp_recurring_default_no_payments_open_filed')))
      ->set('cnpuber.cnp_recurring_max_no_payment_open_filed_lbl', trim($form_state->getValue('cnp_recurring_max_no_payment_open_filed_lbl')))
      ->set('cnpuber.cnp_recurring_max_no_payment_open_filed', trim($form_state->getValue('cnp_recurring_max_no_payment_open_filed')))
      ->set('cnpuber.cnp_recurring_default_no_payment_fncc_lbl', trim($form_state->getValue('cnp_recurring_default_no_payment_fncc_lbl')))
      ->set('cnpuber.cnp_recurring_default_no_payments_fnnc', trim($form_state->getValue('cnp_recurring_default_no_payments_fnnc')))
      ->save();
  }

  /**
   * Create cnp payment setting form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param mixed $form_state
   *   The current state of the form.
   * @param mixed $config
   *   Configuration Object.
   *
   * @return array
   *   Returns The form structure.
   */
  public function displaySettingsForm($form, $config, $form_state) {
    // Query to get loggedin user data.
    $query = $this->connection->query("SELECT cnptokeninfo_username FROM {dp_cnp_uber_jbcnptokeninfo}");
    $result = $query->fetchAll();
    $loggedinAs = $result[0]->cnptokeninfo_username;

    $form['cnp_bform_main_div_start'] = [
      '#prefix' => '<div class="cnp_bform_main_div">',
    ];

    // Logo display.
    $modulePath = $this->moduleHandler->getModule('clickandpledge_ubercart')->getPath();
    $cnpdlogo = "<img src='" . base_path() . $modulePath . "/images/cnp_logo.png'>";
    $form['cnp_dlogo'] = [
      '#prefix' => '<div class="cnp_dlogo"> ' . $cnpdlogo,
      '#suffix' => '</div>',
    ];

    $form['heading_text'] = [
      '#markup' => '<div>
			<p>Click & Pledge works by adding credit card fields on the checkout and then sending the details to Click & Pledge for verification.</p>
			<p>You are logged in as <b>[' . $loggedinAs . ']</b></p>
			<a class="button" href="cnpauth">Change User</a></div><br><hr/>',
    ];
    $form['base_url_cnp'] = [
      '#type' => 'hidden',
      '#default_value' => base_path(),
      '#attributes' => ['id' => 'base_url_cnp'],
    ];
    $form['cnp_status_enable'] = [
      '#type' => 'checkboxes',
      '#title' => 'Status',
      '#attributes' => ['id' => 'enable-disable-cnp-payment-gateway'],
      "#options" => ['yes' => 'Enable Click & Pledge'],
      '#default_value' => ($config->get('cnpuber.cnp_status_enable')) ? $config->get('cnpuber.cnp_status_enable') : [],
      '#prefix' => '<div class="container-inline cnp_bform_status_check">',
      '#suffix' => '</div>',
    ];
    $form['hs_div_start'] = [
      '#prefix' => '<div id="cnp_hs_div">',
    ];

    $form['cnp_accid'] = [
      '#type' => 'select',
      '#required' => TRUE,
      '#title' => t('C&P Account ID'),
      '#default_value' => $config->get('cnpuber.cnp_accid'),
      '#prefix' => '<div id="cnp_accid_wrapper" class="cnp_bform_accid">',
      '#suffix' => '</div>',
      '#options' => $this->getAccountIDs(),
      '#ajax' => [
        'callback' => [$this, 'changeOptionsAjax'],
        'wrapper' => 'cnp_camp_urls_wrapper',
      ],
    ];
    $form['cnp_accid_hidden'] = [
      '#type' => 'hidden',
      '#attributes' => ['id' => 'cnp_accid_hidden'],
      '#default_value' => $config->get('cnpuber.cnp_accid'),
    ];

    $form['refresh_accounts'] = [
      '#markup' => '<a href="#" id="rfrshtokens">Refresh Accounts</a>',
    ];
    // Onload display campaign url of the selected account ID.
    $form['cnp_camp_urls'] = [
      '#type' => 'select',
      '#title' => t('CONNECT Campaign URL Alias<div class="cnp_demo">
  <span data-tooltip="Transaction will post to this CONNECT campaign. Receipts, stats are sent and updated based on the set campaign" class="cnp_tooltip">?</span></div>'),
      '#default_value' => $config->get('cnpuber.cnp_camp_urls'),
      '#options' => $this->getOptions($form_state, $config),
      '#prefix' => '<div id="cnp_camp_urls_wrapper" class="cnp_bform_camp_urls">',
      '#suffix' => '</div>',
    ];
    $form['cnp_camp_urls_tooltip'] = [
      '#markup' => '',
    ];
    $form['cnp_mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('Transaction Mode'),
      '#options' => [
        'Yes' => $this->t('Test'),
        'No' => $this->t('Production'),
      ],
      '#default_value' => ($config->get('cnpuber.cnp_mode') !== NULL) ? $config->get('cnpuber.cnp_mode') : 'No',
      '#prefix' => '<div class="container-inline cnp_bform_mode">',
      '#suffix' => '</div>',
    ];
    // PAYMENT METHODS.
    $form['cnp_payment_credit_cards'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Payment Methods'),
      '#attributes' => [
        'checked' => 'checked',
        'disabled' => 'disabled',
      ],
      '#options' => ['Credit Cards' => 'Credit Cards'],
      '#default_value' => ($config->get('cnpuber.cnp_payment_credit_cards')) ? $config->get('cnpuber.cnp_payment_credit_cards') : [],
      '#prefix' => '<div class="container-inline cnp_bform_payment_cards">',
      '#suffix' => '</div>',
    ];
    // Get the payment options once the page has loaded.
    if ($config->get('cnpuber.cnp_accid')) {
      $serviceOPtions = $this->getWCCnPactivePaymentList($config->get('cnpuber.cnp_accid'));
      $sOpt = ['Amex', 'Discover', 'Jcb', 'Master', 'Visa'];
      if (!empty($serviceOPtions)) {
        $OriginalOptions = [];
        $payOptions = $serviceOPtions->GetAccountDetailResult;
        foreach ($payOptions as $k => $v) {
          if (in_array($k, $sOpt)) {
            if ($v === 1) {
              $OriginalOptions[$k] = $k;
            }
          }
        }
      }
      else {
        $OriginalOptions = ['Amex', 'Discover', 'Jcb', 'Master', 'Visa'];
      }
    }
    else {
      $OriginalOptions = ['Amex', 'Discover', 'Jcb', 'Master', 'Visa'];
    }
    $form['cnp_payment_credit_card_options'] = [
      '#prefix' => '<div class="container-inline cnp_payment_options_wrapper" id="payment_options_wrapper">',
      '#suffix' => '</div>',
    ];

    $cards_hidden = '';
    foreach ($OriginalOptions as $op) {
      $cards_hidden .= $op . "#";
    }
    $form['cnp_payment_credit_card_options_hidden'] = [
      '#type' => 'hidden',
      '#attributes' => ['id' => 'card_options_hidden'],
      '#default_value' => $cards_hidden,
      '#prefix' => '<div class="" id="credit_card_options_hidden">',
      '#suffix' => '</div>',
    ];

    $form['note_text'] = [
      '#markup' => '<div>
			<p><b>Note:</b> Due to limitations with the Drupal Ubercart payment API, only Credit Card payment is currently supported.</p></div>',
    ];

    $form['cnp_pre_auth'] = [
      '#type' => 'hidden',
      '#default_value' => 1,
    ];

    $form['receipt_settings_start'] = [
      '#prefix' => '<div class="cnp_bform_receipt_settings">',
    ];

    $form['some_text'] = [
      '#markup' => '<H2>Receipt Settings</H2>',
    ];
    $form['cnp_receipt_patron'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Send Receipt to Patron'),
      '#options' => ['1' => 'Send Receipt to Patron'],
      '#default_value' => ($config->get('cnpuber.cnp_receipt_patron')) ? $config->get('cnpuber.cnp_receipt_patron') : [],
      '#prefix' => '<div class="cnp_bform_receipt_patron">',
      '#suffix' => '</div>',
    ];
    $form['cnp_receipt_header'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Receipt Header'),
      '#attributes' => [
        'maxlength' => '1500',
        'id' => 'cnp_receipt_head_msg',
      ],
      '#default_value' => $config->get('cnpuber.cnp_receipt_header'),
      '#prefix' => '<div class="cnp_bform_receipt_header">',
      '#suffix' => '</div>',
      '#description' => "Maximum: 1500 characters, the following HTML tags are allowed: " . htmlspecialchars("<P></P><BR /><OL></OL><LI></LI><UL></UL>") . ". you have <span id='cnpheadcount'>1500</span> characters left.",
    ];
    $form['cnp_terms_con'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Terms & Conditions'),
      '#attributes' => [
        'maxlength' => '1500',
        'id' => 'cnp_terms_con_msg',
      ],
      '#default_value' => $config->get('cnpuber.cnp_terms_con'),
      '#prefix' => '<div class="cnp_bform_terms_con">',
      '#suffix' => '</div>',
      '#description' => "The following HTML tags are allowed: " . htmlspecialchars("<P></P><BR /><OL></OL><LI></LI><UL></UL>") . ". 
Maximum: 1500 characters, you have <span id='cnptnccount'>1500</span> characters left.",
    ];
    $form['receipt_settings_end'] = [
      '#suffix' => '</div>',
    ];

    $form['some_text1'] = [
      '#markup' => '<H2 class="recurring_set">Recurring Settings</H2>',
    ];
    $form['cnp_recurr_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#default_value' => ($config->get('cnpuber.cnp_recurr_label')) ? $config->get('cnpuber.cnp_recurr_label') : "Set this as a recurring payment",
      '#prefix' => '<div class="cnp_bform_recurr_label">',
      '#suffix' => '</div>',
    ];
    $form['cnp_recurr_settings'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Settings'),
      '#default_value' => ($config->get('cnpuber.cnp_recurr_settings')) ? $config->get('cnpuber.cnp_recurr_settings') : "Payment options",
      '#prefix' => '<div class="cnp_bform_recurr_settings">',
      '#suffix' => '</div>',
    ];
    $form['cnp_recurr_oto'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t(''),
      '#options' => ['oto' => 'One Time Only'],
      '#default_value' => ($config->get('cnpuber.cnp_recurr_oto')) ? $config->get('cnpuber.cnp_recurr_oto') : [],
      '#prefix' => '<div class="cnp_recurr_oto">',
      '#suffix' => '</div>',
    ];
    $form['cnp_recurr_recur'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t(''),
      '#options' => ['1' => 'Recurring'],
      '#default_value' => ($config->get('cnpuber.cnp_recurr_recur')) ? $config->get('cnpuber.cnp_recurr_recur') : [],
      '#prefix' => '<div class="cnp_recurr_recur">',
      '#suffix' => '</div>',
    ];

    $form['recurr_option_div_start'] = [
      '#markup' => '<div class="recurr_option">',
    ];

    $form['cnp_default_payment_options'] = [
      '#type' => 'select',
      '#title' => $this->t('Default Payment Options'),
      '#options' => [
        'Recurring' => $this->t('Recurring'),
        'One Time Only' => $this->t('One Time Only'),
      ],
      '#prefix' => '<div class="container-inline cnp_bform_default_payment_options" id="default_payment_options_wrapper">',
      '#suffix' => '</div>',
      '#default_value' => $config->get('cnpuber.cnp_default_payment_options'),
    ];
    $form['cnp_recurring_types'] = [
      '#type' => 'textfield',
      '#title' => $this->t(''),
      '#prefix' => '<div class="cnp_bform_recurring_types">',
      '#suffix' => '</div>',
      '#default_value' => ($config->get('cnpuber.cnp_recurring_types')) ? $config->get('cnpuber.cnp_recurring_types') : "Recurring types",
    ];
    $form['cnp_recurr_type_option'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t(''),
      '#options' => [
        'Installment' => 'Installment (e.g. pay $1000 in 10 installments of $100 each)',
        'Subscription' => 'Subscription (e.g. pay $100 every month for 12 months)',
      ],
      '#default_value' => ($config->get('cnpuber.cnp_recurr_type_option')) ? $config->get('cnpuber.cnp_recurr_type_option') : ["Installment" => "Installment (e.g. pay $1000 in 10 installments of $100 each)"],
      '#prefix' => '<div class="cnp_bform_recurr_type_option">',
      '#suffix' => '</div>',
    ];
    $form['cnp_default_recurring_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Default Recurring type'),
      '#options' => [
        'Subscription' => $this->t('Subscription'),
        'Installment' => $this->t('Installment'),
      ],
      '#prefix' => '<div class="container-inline cnp_bform_default_recurring_type" id="default_recurring_type_wrapper">',
      '#suffix' => '</div>',
      '#default_value' => $config->get('cnpuber.cnp_default_recurring_type'),
    ];
    $form['cnp_recurring_periodicity'] = [
      '#type' => 'textfield',
      '#title' => $this->t(''),
      '#prefix' => '<div class="cnp_bform_recurring_periodicity">',
      '#suffix' => '</div>',
      '#default_value' => ($config->get('cnpuber.cnp_recurring_periodicity')) ? $config->get('cnpuber.cnp_recurring_periodicity') : "Periodicity",
    ];
    $form['cnp_recurring_periodicity_options'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t(''),
      '#options' => [
        'Week' => 'Week',
        '2 Weeks' => '2 Weeks',
        'Month' => 'Month',
        '2 Months' => '2 Months',
        'Quarter' => 'Quarter',
        '6 Months' => '6 Months',
        'Year' => 'Year',
      ],
      '#default_value' => ($config->get('cnpuber.cnp_recurring_periodicity_options')) ? $config->get('cnpuber.cnp_recurring_periodicity_options') : [],
      '#prefix' => '<div class="cnp_bform_recurring_periodicity_options">',
      '#suffix' => '</div>',
    ];
    $form['cnp_recurring_no_of_payments'] = [
      '#type' => 'textfield',
      '#prefix' => '<div class="cnp_bform_recurring_no_of_payments">',
      '#default_value' => ($config->get('cnpuber.cnp_recurring_no_of_payments')) ? $config->get('cnpuber.cnp_recurring_no_of_payments') : "Number of payments",
    ];
    $form['cnp_recurring_no_of_payments_options'] = [
      '#type' => 'radios',
      '#title' => $this->t(''),
      '#options' => [
        'indefinite_openfield' => 'Indefinite + Open Field Option',
        '1' => 'Indefinite Only',
        'openfield' => 'Open Field Only',
        'fixednumber' => 'Fixed Number - No Change Allowed',
      ],
      '#suffix' => '</div>',
      '#default_value' => $config->get('cnpuber.cnp_recurring_no_of_payments_options'),
    ];
    $form['cnp_recurring_default_no_payment_lbl'] = [
      '#type' => 'textfield',
      '#prefix' => '<div class="default_no_of_payments_wrapper_start"><div class="container-inline cnp_bform_no_payment_lbl">',
      '#default_value' => ($config->get('cnpuber.cnp_recurring_default_no_payment_lbl')) ? $config->get('cnpuber.cnp_recurring_default_no_payment_lbl') : "Default number of payments",
    ];
    $form['cnp_recurring_default_no_payments'] = [
      '#type' => 'textfield',
      '#title' => $this->t(''),
      '#attributes' => ['maxlength' => 3],
      '#suffix' => '</div>',
      '#default_value' => ($config->get('cnpuber.cnp_recurring_default_no_payments')) ? $config->get('cnpuber.cnp_recurring_default_no_payments') : "",
    ];
    $form['cnp_recurring_max_no_payment_open_filed_lbl'] = [
      '#type' => 'textfield',
      '#prefix' => '<div class="container-inline cnp_bform_fix_number_no_change" id="fix-number-no-change">',
      '#default_value' => ($config->get('cnpuber.cnp_recurring_max_no_payment_open_filed_lbl')) ? $config->get('cnpuber.cnp_recurring_max_no_payment_open_filed_lbl') : "Maximum number of installments allowed",
    ];
    $form['cnp_recurring_max_no_payment'] = [
      '#type' => 'textfield',
      '#title' => $this->t(''),
      '#attributes' => ['maxlength' => 3],
      '#suffix' => '</div></div>',
      '#default_value' => ($config->get('cnpuber.cnp_recurring_max_no_payment')) ? $config->get('cnpuber.cnp_recurring_max_no_payment') : "",
    ];
    // Open filed form.
    $form['cnp_recurring_default_no_payment_open_field_lbl'] = [
      '#type' => 'textfield',
      '#prefix' => '<div class="open_filed_wrapper_start"><div class="container-inline cnp_bform_open_field_lbl">',
      '#default_value' => ($config->get('cnpuber.cnp_recurring_default_no_payment_open_field_lbl')) ? $config->get('cnpuber.cnp_recurring_default_no_payment_open_field_lbl') : "Default number of payments",
    ];

    $form['cnp_recurring_default_no_payments_open_filed'] = [
      '#type' => 'textfield',
      '#title' => $this->t(''),
      '#attributes' => ['maxlength' => 3],
      '#default_value' => ($config->get('cnpuber.cnp_recurring_default_no_payments_open_filed')) ? $config->get('cnpuber.cnp_recurring_default_no_payments_open_filed') : "",
      '#suffix' => '</div>',
    ];
    $form['cnp_recurring_max_no_payment_lbl'] = [
      '#type' => 'textfield',
      '#prefix' => '<div class="container-inline cnp_bform_max_no_payment" id="">',
      '#default_value' => ($config->get('cnpuber.cnp_recurring_max_no_payment_lbl')) ? $config->get('cnpuber.cnp_recurring_max_no_payment_lbl') : "Maximum number of installments allowed",
    ];

    $form['cnp_recurring_max_no_payment_open_filed'] = [
      '#type' => 'textfield',
      '#title' => $this->t(''),
      '#attributes' => [
        'disabled' => 'disabled',
        'maxlength' => 3,
      ],
      '#suffix' => '</div></div>',
      '#default_value' => ($config->get('cnpuber.cnp_recurring_max_no_payment_open_filed')) ? $config->get('cnpuber.cnp_recurring_max_no_payment_open_filed') : "999",
    ];
    // Fixed Number nochange allowed filed.
    $form['cnp_recurring_default_no_payment_fncc_lbl'] = [
      '#type' => 'textfield',
      '#prefix' => '<div class="fixed_number_no_chnage_wrapper_start"><div class="container-inline cnp_bform_no_change">',
      '#default_value' => ($config->get('cnpuber.cnp_recurring_default_no_payment_fncc_lbl')) ? $config->get('cnpuber.cnp_recurring_default_no_payment_fncc_lbl') : "Default number of payments",
    ];
    $form['cnp_recurring_default_no_payments_fnnc'] = [
      '#type' => 'textfield',
      '#title' => $this->t(''),
      '#attributes' => ['maxlength' => 3],
      '#suffix' => '</div></div>',
      '#default_value' => ($config->get('cnpuber.cnp_recurring_default_no_payments_fnnc')) ? $config->get('cnpuber.cnp_recurring_default_no_payments_fnnc') : "",
    ];

    $form['recurr_option_div_end'] = [
      '#markup' => '</div>',
    ];
    $form["hs_div_end"] = [
      '#suffix' => '</div>',
    ];

    return $form;
  }

  /**
   * Get All accound iD's.
   *
   * @return array
   *   Returns an array with accounts.
   */
  public function getAccountIDs() {
    $query = $this->connection->query("SELECT cnpaccountsinfo_orgid,cnpaccountsinfo_orgname FROM {dp_cnp_uber_jbcnpaccountsinfo}");
    $result = $query->fetchAll();
    $opt = [];
    $opt[''] = '-select-';
    foreach ($result as $acc) {
      $opt[$acc->cnpaccountsinfo_orgid] = $acc->cnpaccountsinfo_orgid . " [" . $acc->cnpaccountsinfo_orgname . "]";
    }
    ksort($opt);
    return $opt;
  }

  /**
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Returns the element for ajax call interaction.
   */
  public function changeOptionsAjax(array &$form, FormStateInterface $form_state) {
    return $form['cnp_camp_urls'];
  }

  /**
   * Refresh accounts.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Returns the element for ajax call interaction.
   */
  public function RefreshAccounts(array &$form, FormStateInterface $form_state) {
    return $form['cnp_accid'];
  }

  /**
   * Get options for second field.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param mixed $config
   *   Configuration object.
   *
   * @return array
   *   Returns an array with options.
   */
  public function getOptions(FormStateInterface $form_state, $config) {
    $camrtrnval[''] = [];
    if ($form_state->getValue('cnp_accid') == '') {

      $camrtrnval[''] = '--Select Campaign Name--';
      $accid = $config->get('cnpuber.cnp_accid');
      $opt = $this->cnp_getCnPUserEmailAccountList($accid)[0];
      if (!empty($opt)) {
        if (is_array($opt)) {
          for ($in = 0; $in < count($opt); $in++) {
            $camrtrnval[$opt[$in]->alias] = $opt[$in]->name . " (" . $opt[$in]->alias . ")";
          }
        }
        else {
          $camrtrnval[$opt->alias] = $opt->name . " (" . $opt->alias . ")";
        }
      }
      else {
        $camrtrnval[''] = 'Campaign URL Alias are not found';
      }

      if ($config->get('cnpuber.cnp_camp_urls') !== '') {

      }
      else {
        $camrtrnval[''] = '--Select Campaign Name--';
      }
    }
    else {
      $optionData = $this->cnp_getCnPUserEmailAccountList($form_state->getValue('cnp_accid'));
      $camrtrnval[''] = '--Select Campaign Name--';
      $cnpsel = '';
      $camrtrnval[''] = '--Select Campaign Name--';
      $options = $optionData[0];

      if (count($options) === 1) {
        $camrtrnval[$options->alias] = $options->name . " (" . $options->alias . ")";
      }
      else {
        for ($inc = 0; $inc < count($options); $inc++) {
          $camrtrnval[$options[$inc]->alias] = $options[$inc]->name . " (" . $options[$inc]->alias . ")";
        }
      }
    }
    natcasesort($camrtrnval);
    return $camrtrnval;
  }

  /**
   * To get users email account list.
   *
   * @param mixed $cnpacid
   *   Account id.
   *
   * @return array
   *   Returns an array with accounts
   */
  public function cnp_getCnPUserEmailAccountList($cnpacid) {
    $cnpwcaccountid = $cnpacid;
    $totRes = [];

    $cnprtrntxt = $this->getwcCnPConnectCampaigns($cnpwcaccountid);
    $totRes[] = $cnprtrntxt;
    $Clist = [];
    $cnprtrnpaymentstxt = $this->getWCCnPactivePaymentList($cnpwcaccountid);
    // print_r($cnprtrnpaymentstxt);
    // if (count($cnprtrnpaymentstxt) > 0) {.
    if (!empty($cnprtrnpaymentstxt)) {
      foreach ($cnprtrnpaymentstxt as $obj => $cli) {
        foreach ($cli as $key => $value) {
          if ($value === 1) {
            $Clist[$key] = $key;
          }
        }
      }
    }
    else {
      $Clist = [];
    }
    $totRes[] = $Clist;
    return $totRes;
  }

  /**
   * To get all CONNECT campaigns.
   *
   * @param mixed $cnpaccid
   *   Account Id.
   *
   * @return array
   *   Returns an array of campaigns list.
   */
  public function getwcCnPConnectCampaigns($cnpaccid) {

    $cnpacountid = $cnpaccid;
    $cnpaccountGUID = $this->getwcCnPAccountGUID($cnpacountid);
    $cnpUID = '14059359-D8E8-41C3-B628-E7E030537905';
    $cnpKey = '5DC1B75A-7EFA-4C01-BDCD-E02C536313A3';
    $connect = [
      'soap_version' => 'SOAP_1_1',
      'trace' => 1,
      'exceptions' => 0,
    ];
    $default = [
      // We shall only enable TRACING & EXCEPTION for dev.
      'trace' => 1,
      'exceptions' => TRUE,
      'cache_wsdl' => WSDL_CACHE_NONE,
      'features' => SOAP_SINGLE_ELEMENT_ARRAYS,
    ];
    $client = new \SoapClient('https://resources.connect.clickandpledge.com/wordpress/Auth2.wsdl', $connect);
    if (isset($cnpacountid) && $cnpacountid !== '' && isset($cnpaccountGUID) && $cnpaccountGUID !== '') {
      $xmlr = new SimpleXMLElement('<GetActiveCampaignList2></GetActiveCampaignList2>');

      $cnpsel = '';
      $xmlr->addChild('accountId', $cnpacountid);
      $xmlr->addChild('AccountGUID', $cnpaccountGUID);
      $xmlr->addChild('username', $cnpUID);
      $xmlr->addChild('password', $cnpKey);

      $response = $client->GetActiveCampaignList2($xmlr);
      $responsearr = $response->GetActiveCampaignList2Result->connectCampaign;
    }
    else {
      $responsearr = [];
    }
    return $responsearr;
  }

  /**
   * To get Active payment list form CONNECT platform.
   *
   * @param mixed $cnpaccid
   *   Account Id.
   *
   * @return mixed
   *   Returns an Object.
   */
  public function getWCCnPactivePaymentList($cnpaccid) {
    $cnpacountid = $cnpaccid;
    $cnpaccountGUID = $this->getwcCnPAccountGUID($cnpacountid);
    $cnpUID = '14059359-D8E8-41C3-B628-E7E030537905';
    $cnpKey = '5DC1B75A-7EFA-4C01-BDCD-E02C536313A3';
    $connect1 = [
      'soap_version' => 'SOAP_1_1',
      'trace' => 1,
      'exceptions' => 0,
    ];
    $client1 = new \SoapClient('https://resources.connect.clickandpledge.com/wordpress/Auth2.wsdl', $connect1);
    if (isset($cnpacountid) && $cnpacountid !== '' && isset($cnpaccountGUID) && $cnpaccountGUID !== '') {
      $xmlr1 = new SimpleXMLElement('<GetAccountDetail></GetAccountDetail>');
      $xmlr1->addChild('accountId', $cnpacountid);
      $xmlr1->addChild('accountGUID', $cnpaccountGUID);
      $xmlr1->addChild('username', $cnpUID);
      $xmlr1->addChild('password', $cnpKey);
      $response1 = $client1->GetAccountDetail($xmlr1);
      return $response1;
    }
  }

  /**
   * To get account GUID.
   *
   * @param mixed $accid
   *   Account id.
   *
   * @return mixed
   *   Returns an array with account GUID.
   */
  public function getwcCnPAccountGUID($accid) {
    $cnpAccountGUId = "";
    $query = $this->connection->query('SELECT cnpaccountsinfo_accountguid FROM {dp_cnp_uber_jbcnpaccountsinfo} where cnpaccountsinfo_orgid =:accid', [':accid' => $accid]);
    $result = $query->fetchAssoc();
    $cnpAccountGUId = $result['cnpaccountsinfo_accountguid'];
    return $cnpAccountGUId;
  }

  /**
   * Enable default payment method.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Returns an array of default payment.
   */
  public function checkDefaultPayments(array &$form, FormStateInterface $form_state) {
    return $form['cnp_default_payment'];
  }

  /**
   * To get payment options.
   *
   * @param mixed $form_state
   *   The current state of the form.
   * @param mixed $config
   *   Configuration object.
   *
   * @return array
   *   Returns an array with payment options.
   */
  public function getPayOptions($form_state, $config) {
    $opt = [];
    if ($form_state->getValue('cnp_payment_methdos1')) {
      $data = $form_state->getValue('cnp_payment_methdos1');
      if ($data['CustomPayment']) {
        if ($form_state->getValue('cnp_customPayment_titles')) {
          $opt['Credit Card'] = 'Credit Card';
          $opt['eCheck'] = 'eCheck';
          $pt = $form_state->getValue('cnp_customPayment_titles');
          $arr = explode(';', $pt, -1);
          foreach ($arr as $a) {
            $opt[$a] = $a;
          }
        }
        else {
          $opt['Credit Card'] = 'Credit Card';
          $opt['eCheck'] = 'eCheck';
          $svaed_pay_methods = $config->get('cnpuber.cnp_customPayment_titles');
          $arr = explode(';', $svaed_pay_methods, -1);
          foreach ($arr as $a) {
            $opt[$a] = $a;
          }
        }
      }
      else {
        $opt['Credit Card'] = 'Credit Card';
        $opt['eCheck'] = 'eCheck';
      }
    }
    else {
      if ($config->get('cnpuber.cnp_payment_methdos1')) {
        $opt['Credit Card'] = 'Credit Card';
        $opt['eCheck'] = 'eCheck';
        $svaed_pay_methods = $config->get('cnpuber.cnp_customPayment_titles');
        $arr = explode(';', $svaed_pay_methods, -1);
        foreach ($arr as $a) {
          $opt[$a] = $a;
        }
      }
      else {
        $opt['Credit Card'] = 'Credit Card';
        $opt['eCheck'] = 'eCheck';
        $opt['COD'] = 'COD';
      }
    }

    return $opt;
  }

  /**
   * {@inheritdoc}
   */
  public function updateDefaultPayments(array &$form, FormStateInterface $form_state) {
    return $form['cnp_default_payment'];
  }

}
