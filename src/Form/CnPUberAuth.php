<?php

namespace Drupal\clickandpledge_ubercart\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Click & Pledge Authentication form.
 */
class CnPUberAuth extends ConfigFormBase {

  /**
   * The Database connection.
   *
   * @var mixed
   */
  protected $connection;

  /**
   * The Module Handler.
   *
   * @var mixed
   */
  protected $moduleHandler;

  /**
   * The class Constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection object.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The Module Handler Object.
   */
  public function __construct(Connection $connection, ModuleHandlerInterface $module_handler) {
    $this->connection = $connection;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'), $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'cnpuber.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return "cnpuber_settings_form";
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config("cnpuber.settings");
    $form = $this->displayBasicForm($form, $config, $form_state);
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config("cnpuber.settings");
    if (!$this->is_valid_email(trim($form_state->getValue('cnp_vemail')))) {
      $form_state->setErrorByName('cnp_vemail', $this->t('Please enter valid Email.'));
    }
    if (!$config->get('cnpuber.cnp_vemail')) {
      if (empty($this->verifyEmailWithConnect($form_state->getValue('cnp_vemail')))) {
        $form_state->setErrorByName('cnp_vemail', $this->t('Please enter registered email with CONNECT.'));
      }
    }
    if (trim($form_state->getValue('cnp_verify_code')) != "") {
      $pat = '/^\d{5}$/';
      if (!preg_match($pat, trim($form_state->getValue('cnp_verify_code')))) {
        $form_state->setErrorByName('cnp_verify_code', $this->t('Enter valid verification code'));
      }
    }
    else {
      if ($config->get('cnpuber.cnp_vemail') != "") {
        $form_state->setErrorByName('cnp_verify_code', $this->t('Enter valid verification code'));
      }
    }

    if (trim($form_state->getValue('cnp_verify_code')) != "" && trim($form_state->getValue('cnp_vemail')) != "") {
      $data = $this->getCnPTransactions(trim($form_state->getValue('cnp_vemail')), trim($form_state->getValue('cnp_verify_code')));
      if ($data == "error") {
        $form_state->setErrorByName('cnp_verify_code', $this->t('Please enter verification code.'));
      }
    }
  }

  /**
   * To check the email is valid email or not.
   *
   * @param mixed $email
   *   The email to be verified.
   *
   * @return mixed
   *   This method returns true, if it is valid email.
   */
  public function is_valid_email($email) {
    return preg_match('/^(([^<>()[\]\\.,;:\s@"\']+(\.[^<>()[\]\\.,;:\s@"\']+)*)|("[^"\']+"))@((\[\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\])|(([a-zA-Z\d\-]+\.)+[a-zA-Z]{2,}))$/', $email);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $cnp_verify_code = ($form_state->getValue('cnp_verify_code')) ? $form_state->getValue('cnp_verify_code') : "";
    $cnp_vemail = ($form_state->getValue('cnp_vemail')) ? $form_state->getValue('cnp_vemail') : "";

    $cnp_verify_code = trim($cnp_verify_code);
    $cnp_vemail = trim($cnp_vemail);

    if ($form_state->getValues('cnp_vemail') != "") {
      if (!$cnp_verify_code) {
        $this->config('cnpuber.settings')
          ->set('cnpuber.cnp_vemail', trim($form_state->getValue('cnp_vemail')))
          ->set('cnpuber.cnp_verify_code', trim($cnp_verify_code))
          ->save();
      }
    }

    if ($cnp_verify_code != "" && $cnp_vemail != "") {
      $this->config('cnpuber.settings')
        ->set('cnpuber.cnp_vemail', trim($form_state->getValue('cnp_vemail')))
        ->set('cnpuber.cnp_verify_code', trim($cnp_verify_code))
        ->save();
      $this->configFactory()->getEditable('cnpuber.mainsettings')->delete();
      $this->clickandpledge_ubercart_goto("cnpuber_settings");
    }
  }

  /**
   * Creates Authentication form.
   *
   * @param mixed $form
   *   An associative array containing the structure of the form.
   * @param mixed $config
   *   The configuration object.
   * @param mixed $form_state
   *   The current state of the form.
   *
   * @return array
   *   Returns the form structure.
   */
  public function displayBasicForm($form, $config, $form_state) {
    $query = $this->connection->query('SELECT cnpaccountsinfo_orgid FROM {dp_cnp_uber_jbcnpaccountsinfo}');
    $query->allowRowCount = TRUE;
    // Logo display
    // Logo display.
    $modulePath = $this->moduleHandler->getModule('clickandpledge_ubercart')->getPath();
    $cnpalogo = "<img src='" . base_path() . $modulePath . "/images/cnp_logo.png'>";
    $form['cnp_alogo'] = [
      '#prefix' => '<div class="cnp_dlogo"> ' . $cnpalogo,
      '#suffix' => '</div>',
    ];

    $form['heading_text_start'] = [
      '#markup' => '<div>
			<p>Click & Pledge works by adding credit card fields on the checkout and then sending the details to Click & Pledge for verification.</p>
			<ol>
				<li>Enter the email address associated with your Click & Pledge account, and click on Get the Code</li>
				<li>Please check your email inbox for the Login Verification Code email</li>
				<li>Enter the provided code and click Login</li>
			</ol>
			</div>',
    ];

    if ($query->rowCount() != 0) {
      $form['heading_text'] = [
        '#markup' => '<div><img src="" height="" width=""/>
				<a class="button" href="cnpuber_settings">Go to settings</a></div><br><hr/>',
      ];
    }

    $form['cnp_vemail'] = [
      '#type' => 'textfield',
      '#title' => $this->t(''),
      '#description' => $this->t('Enter CONNECT User Name'),
      '#default_value' => $config->get('cnpuber.cnp_vemail'),
    ];

    if ($config->get('cnpuber.cnp_vemail') != "") {
      $form['cnp_verify_code'] = [
        '#type' => 'textfield',
        '#attributes' => ["id" => "verifycode"],
        '#title' => $this->t(''),
        '#description' => $this->t('Please enter the code sent to your email'),
        '#default_value' => $config->get('cnpuber.cnp_verify_code'),
      ];

      $form['swdu_text'] = [
        '#markup' => '<div class="signin_with_diff_user">
				<a class="" href="different_user_signin">Sign in with a different account</a></div>',
      ];
    }

    return $form;
  }

  /**
   * To redirect a page.
   *
   * @param mixed $path
   *   The Path to redirect.
   */
  public function clickandpledge_ubercart_goto($path) {
    $response = new RedirectResponse($path, 302);
    $response->send();
  }

  /**
   * The get the transactions.
   *
   * @param mixed $cnpemailid
   *   The email ID.
   * @param mixed $cnpcode
   *   The code.
   *
   * @return string
   *   Return success or error.
   */
  public function getCnPTransactions($cnpemailid, $cnpcode) {
    $curl = curl_init();
    curl_setopt_array($curl, [
      CURLOPT_URL => "https://aaas.cloud.clickandpledge.com/idserver/connect/token",
      CURLOPT_RETURNTRANSFER => TRUE,
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => $this->get_cnpwctransactions($cnpemailid, $cnpcode),
      CURLOPT_HTTPHEADER => [
        "cache-control: no-cache",
        "content-type: application/x-www-form-urlencoded",
      ],
    ]);

    $response = curl_exec($curl);
    $err = curl_error($curl);
    // Return $response;.
    curl_close($curl);
    if ($err) {
      echo "cURL Error #:" . $err;
    }
    else {
      $cnptokendata = json_decode($response);

      if (!isset($cnptokendata->error)) {

        $cnptoken = (isset($cnptokendata->access_token)) ? $cnptokendata->access_token : '';
        $cnprtoken = (isset($cnptokendata->refresh_token)) ? $cnptokendata->refresh_token : '';

        $cnptransactios = $this->delete_cnpwctransactions();
        $rtncnpdata = $this->insrt_cnpwctokeninfo($cnpemailid, $cnpcode, $cnptoken, $cnprtoken);

        if ($rtncnpdata != "") {
          $curl = curl_init();

          curl_setopt_array($curl, [
            CURLOPT_URL => "https://api.cloud.clickandpledge.com/users/accountlist",
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => [
              "accept: application/json",
              "authorization: Bearer " . $cnptoken,
              "content-type: application/json",
            ],
          ]);

          $response = curl_exec($curl);
          $err = curl_error($curl);
          curl_close($curl);
          if ($err) {
            echo "cURL Error #:" . $err;
          }
          else {
            $cnpAccountsdata = json_decode($response);
            /* echo '<pre>';
            print_r($cnpAccountsdata);
            var_dump($cnpAccountsdata);
            print_r($cnpAccountsdata->Message);
            exit;*/
            if (is_object($cnpAccountsdata)) {
              if (property_exists($cnpAccountsdata, "Message")) {
                return 'error';
              }
            }
            $cnptransactios = $this->delete_wccnpaccountslist();
            foreach ($cnpAccountsdata as $cnpkey => $cnpvalue) {
              $cnporgid = $cnpvalue->OrganizationId;
              $cnporgname = addslashes($cnpvalue->OrganizationName);
              $cnpaccountid = $cnpvalue->AccountGUID;
              $cnpufname = addslashes($cnpvalue->UserFirstName);
              $cnplname = addslashes($cnpvalue->UserLastName);
              $cnpuid = $cnpvalue->UserId;
              $cnptransactios = $this->insert_cnpwcaccountsinfo($cnporgid, $cnporgname, $cnpaccountid, $cnpufname, $cnplname, $cnpuid);
            }
            return 'success';

          }
        }
      }
      else {
        return "error";
      }
    }
  }

  /**
   * To get the settings info.
   *
   * @param mixed $cnpemailid
   *   The user email.
   * @param mixed $cnpcode
   *   The user code.
   *
   * @return string
   *   Returns a string with account information.
   */
  public function get_cnpwctransactions($cnpemailid, $cnpcode) {
    $query = $this->connection->query("SELECT cnpsettingsinfo_clentsecret,cnpsettingsinfo_clientid,cnpsettingsinfo_granttype,cnpsettingsinfo_scope FROM {dp_cnp_uber_jbcnpsettingsinfo}");
    $results = $query->fetchAssoc();
    $count = sizeof($results);
    for ($i = 0; $i < $count; $i++) {
      $password = "password";
      $cnpsecret = openssl_decrypt($results['cnpsettingsinfo_clentsecret'], "AES-128-ECB", $password);
      $rtncnpdata = "client_id=" . $results['cnpsettingsinfo_clientid'] . "&client_secret=" . $cnpsecret . "&grant_type=" . $results['cnpsettingsinfo_granttype'] . "&scope=" . $results['cnpsettingsinfo_scope'] . "&username=" . $cnpemailid . "&password=" . $cnpcode;
    }
    return $rtncnpdata;
  }

  /**
   * Deletes the token informations.
   */
  public function delete_cnpwctransactions() {
    $this->connection->delete('dp_cnp_uber_jbcnptokeninfo')->execute();
  }

  /**
   * To save the token information related to the account.
   *
   * @param mixed $cnpemailid
   *   The User Email.
   * @param mixed $cnpcode
   *   The user code.
   * @param mixed $cnptoken
   *   The access Token.
   * @param mixed $cnprtoken
   *   The refresh token.
   *
   * @return int
   *   Returns inserted id.
   */
  public function insrt_cnpwctokeninfo($cnpemailid, $cnpcode, $cnptoken, $cnprtoken) {
    $fields = [
      'cnptokeninfo_username' => $cnpemailid,
      'cnptokeninfo_code' => $cnpcode,
      'cnptokeninfo_accesstoken' => $cnptoken,
      'cnptokeninfo_refreshtoken' => $cnprtoken,
    ];
    $this->connection->insert('dp_cnp_uber_jbcnptokeninfo')->fields($fields)->execute();
    $res = $this->connection->query("select max(cnptokeninfo_id) from {dp_cnp_uber_jbcnptokeninfo}");
    $id = $res->fetchCol();
    return $id[0];
  }

  /**
   * To delete the account information.
   */
  public function delete_wccnpaccountslist() {
    $this->connection->delete('dp_cnp_uber_jbcnpaccountsinfo')->execute();
  }

  /**
   * To save accounts information.
   *
   * @param mixed $cnporgid
   *   The organization id.
   * @param mixed $cnporgname
   *   The organization name.
   * @param mixed $cnpaccountid
   *   The account id.
   * @param mixed $cnpufname
   *   User First Name.
   * @param mixed $cnplname
   *   Users Last Name.
   * @param mixed $cnpuid
   *   User Id.
   *
   * @return Number
   *   Returns inserted ID.
   */
  public function insert_cnpwcaccountsinfo($cnporgid, $cnporgname, $cnpaccountid, $cnpufname, $cnplname, $cnpuid) {
    $this->connection->insert('dp_cnp_uber_jbcnpaccountsinfo')
      ->fields([
        'cnpaccountsinfo_orgid' => $cnporgid,
        'cnpaccountsinfo_orgname' => $cnporgname,
        'cnpaccountsinfo_accountguid' => $cnpaccountid,
        'cnpaccountsinfo_userfirstname' => $cnpufname,
        'cnpaccountsinfo_userlastname' => $cnplname,
        'cnpaccountsinfo_userid' => $cnpuid,
      ])
      ->execute();
    $res = $this->connection->query("select max(cnpaccountsinfo_id) from {dp_cnp_uber_jbcnpaccountsinfo}");
    $id = $res->fetchCol();
    return $id[0];
  }

  /**
   * Verify the email is exists or not with connect.
   *
   * @param mixed $email
   *   The use email to verify.
   *
   * @return Object
   *   Returns the response as an object.
   */
  public function verifyEmailWithConnect($email) {
    $curl = curl_init();
    curl_setopt_array($curl, [
      CURLOPT_URL => "https://api.cloud.clickandpledge.com/users/requestcode",
      CURLOPT_RETURNTRANSFER => TRUE,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => [
        "cache-control: no-cache",
        "content-type: application/x-www-form-urlencoded",
        "email: " . $email,
      ],
    ]);
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    if ($err) {
      return "cURL Error #:" . $err;
    }
    else {
      return $response;
    }
  }

}
